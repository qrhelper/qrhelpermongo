// qrhelpermongo - QR Code Router/Forwarded in Go part of the QR Helper Application
// Copyright (C) 2016  James LaPointe
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

db.createCollection( "res",
   {
      validator: { $or:
         [
            { uuid: { $type: "string" } },
            { description: { $type: "string" } },
            { name: { $type: "string" } },
            { orgid: { $type: "string" } },
            { protected: { $in: [ "true", "false" ] } },
            { action: { $in: [ "proxy", "redirect", "s3serve", "s3redirect"  ] } },
            { address: { $type: "string" }}
         ]
      },
      validationAction: "warn"
   }
)

db.createCollection( "userrequests",
   {
      validator: { $or:
         [
            { orgName: { $type: "string" } },
            { userName: { $type: "string" } },
            { email: { $type: "string" } }
         ]
      },
      validationAction: "warn"
   }
)


db.createCollection( "orgusers",
   {
      validator: { $or:
         [
            { uuid: { $type: "string" } },
            { description: { $type: "string" } },
            { uuid: { $type: "string" } },
            { protected: { $in: [ "true", "false" ] } },
            { action: { $in: [ "forward", "redirect", "s3serve", "s3redirect" ] } },
            { address: ""}
         ]
      },
      validationAction: "warn"
   }
)


var newObjId1 = new ObjectId
var newObjId2 = new ObjectId

db.orgusers.insert(
{
  "_id": newObjId1,
  "orgname": "organization1",
  "address": "organization1",
  "city": "organization1",
  "state": "organization1",
  "postalcode": "organization1",
  "users": [
    {
      "username": "test2",
      "email": "test@yahoo.com",
      "name": "james lapointe",
      "password": "hash1"
    },
    {
      "username": "test3",
      "email": "test3@yahoo.com",
      "name": "jose lapointe",
      "password": "hash2"
    },
    {
      "username": "test4",
      "email": "test4@yahoo.com",
      "name": "jones lapointe",
      "password": "hash2"
    },
    {
      "username": "test5",
      "email": "test5@yahoo.com",
      "name": "jaquene lapointe",
      "password": "hash2"
    }
  ]
}
)

db.orgusers.insert(
{
  "_id": newObjId2,
  "orgname": "organization2",
  "address": "organization2",
  "city": "organization2",
  "state": "organization2",
  "postalcode": "organization2",
  "users": [
    {
      "username": "org2test1",
      "email": "test@yahoo.com",
      "name": "fooey shoey",
      "password": "hash1"
    },
    {
      "username": "org2test",
      "email": "test3@yahoo.com",
      "name": "jose lapointe",
      "password": "hash2"
    }
  ]
}
)


db.res.insert(
  {
  	"uuid": "034c1dd2-d454-11e6-a110-b34e9f2c654a",
  	"description": "test",
    "name": "yahoo resource",
  	"protected": "false",
  	"orgid": newObjId1,
  	"action": "redirect",
  	"address": "https://www.yahoo.com",
    "accessCount": 1
  }
  )
  db.res.insert(
{
  	"uuid": "059edd7c-d454-11e6-92b9-374c2fc3d623",
  	"description": "test",
    "name": "yahoo resource",
  	"protected": "false",
  	"orgid": newObjId1,
  	"action": "redirect",
  	"address": "https://www.google.com",
    "accessCount": 1
  }
)
db.res.insert(
{
  "uuid": "059edd7c-d454-11e6-92b9-374c2fc3d624",
  "description": "test",
  "name": "yahoo resource",
  "protected": "false",
  "orgid": newObjId1,
  "action": "proxy",
  "address": "https://www.google.com",
  "accessCount": 1
}
)
db.res.insert(
 {
  	"uuid": "06953884-d454-11e6-88d7-7f0693261746",
  	"description": "test",
    "name": "yahoo resource",
  	"protected": "false",
  	"orgid": newObjId1,
  	"action": "proxy",
  	"address": "https://www.linkedin.com",
    "accessCount": 1
  }
)
db.res.insert(
 {
  	"uuid": "06953884-d454-11e6-88d7-7f0693261747",
  	"description": "test",
    "name": "yahoo resource",
  	"protected": "false",
  	"orgid": newObjId1,
  	"action": "redirect",
  	"address": "https://www.linkedin.com",
    "accessCount": 1
  }
)
db.res.insert(
{
  	"uuid": "a9a3ef4c-d455-11e6-8ca2-8bbb8b59273d",
  	"description": "test",
    "name": "yahoo resource",
  	"protected": "false",
  	"orgid": newObjId1,
  	"action": "redirect",
  	"address": "http://localhost:4567/sample-md-file",
    "accessCount": 1
  }
)
db.res.insert(
{
  	"uuid": "a9a3ef4c-d455-11e6-8ca2-8bbb8b59273e",
  	"description": "test",
    "name": "yahoo resource",
  	"protected": "false",
  	"orgid": newObjId1,
  	"action": "proxy",
  	"address": "http://localhost:26000/sample-md-file",
    "accessCount": 1
  }
)
db.res.insert(
{
  	"uuid": "ac0c5094-d455-11e6-80e5-436a8bb51044",
  	"description": "test",
    "name": "yahoo resource",
  	"protected": "false",
  	"orgid": newObjId1,
  	"action": "redirect",
  	"address": "http://localhost:81/Homesdf",
    "accessCount": 1
  }
)
db.res.insert(
{
  	"uuid": "aeb90c38-d455-11e6-83ef-9ba086d72a0a",
  	"description": "test",
    "name": "yahoo resource",
  	"protected": "false",
  	"orgid": newObjId1,
  	"action": "proxy",
  	"address": "https://www.google.com",
    "accessCount": 1
  }
)
db.res.insert(
{
  	"uuid": "S3b90c38-d455-11e6-83ef-9ba086d72a0a",
  	"description": "test",
    "name": "yahoo resource",
  	"protected": "false",
  	"orgid": newObjId1,
  	"action": "s3serve",
  	"address": "es-lf51_mul_om.PDF",
    "accessCount": 1
}
)
db.res.insert(
{
  	"uuid": "S4b90c38-d455-11e6-83ef-9ba086d72a0a",
  	"description": "test",
    "name": "manual.pdf",
  	"protected": "false",
  	"orgid": newObjId1,
  	"action": "s3redirect",
  	"address": "es-lf51_mul_om.PDF",
    "accessCount": 1
}
)
